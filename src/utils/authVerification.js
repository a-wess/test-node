import jsonwebtoken from 'jsonwebtoken'

function authVerification (req, res, next) {
  const { authorization } = req.headers
  if (!authorization) {
    return res.status(403).send({ message: 'Unauthorized access' })
  }
  const token = authorization.split(' ')[1]
  try {
    const decode = jsonwebtoken.verify(token, process.env.JWT_SECRET)
    req.userId = decode.id
  } catch (err) {
    return res.status(403).send({ message: err.message })
  }
  next()
}

export default authVerification
