const dash = {}

dash.pick = (object, keys) => {
  return keys.reduce((obj, key) => {
    if (object && Object.prototype.hasOwnProperty.call(object, key)) {
      obj[key] = object[key]
    }
    return obj
  }, {})
}

dash.isString = (str) => {
  if (str != null && typeof str.valueOf() === 'string') {
    return true
  }
  return false
}

export default dash
