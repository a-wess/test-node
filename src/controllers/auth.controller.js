import User from '../models/user.model.js'
import crypto from 'crypto'
import jsonwebtoken from 'jsonwebtoken'
import isValidEmail from '../utils/emailValidation.js'
import _ from '../utils/dash.js'

function passHash (password) {
  return crypto.createHash('sha256').update(password).digest('base64')
}

function validateRegistrationData (user) {
  if (!user.name || !_.isString(user.name)) throw new Error('Invalid name')
  if (!user.lastName || !_.isString(user.lastName)) throw new Error('Invalid lastname')
  if (!user.email || !isValidEmail(user.email)) throw new Error('Invalid Email')
  if (!user.password || !_.isString(user.password)) throw new Error('Invalid password')
}

function validateAuthData (user) {
  if (!user.email || !isValidEmail(user.email)) throw new Error('Invalid email')
  if (!user.password || !_.isString(user.password)) throw new Error('Invalid password')
}

const register = async (req, res) => {
  const regData = req.body
  try {
    validateRegistrationData(regData)
  } catch (err) {
    return res.status(422).send({ message: err.message })
  }
  try {
    const existingUser = await User.getByMail(regData.email)
    if (existingUser) {
      return res.status(409).send({ message: `User ${existingUser.email} already exists` })
    }
    const newUser = await User.create({
      ...regData,
      password: passHash(regData.password)
    })
    const token = jsonwebtoken.sign(
      { id: newUser.id },
      process.env.JWT_SECRET,
      { expiresIn: '600s' }
    )
    return res.status(201).send({
      ..._.pick(newUser, ['id', 'name', 'lastName', 'email']),
      token
    })
  } catch (err) {
    res.status(500).send({ message: err.message })
  }
}

const login = async (req, res) => {
  const authData = req.body

  try {
    validateAuthData(authData)
  } catch (err) {
    return res.status(422).send({ message: err.message })
  }

  const user = await User.getByMail(authData.email)
  if (!user || passHash(authData.password) !== user.password) {
    return res.status(403).send({ message: 'Incorrect email or password' })
  } else {
    const token = jsonwebtoken.sign(
      { id: user.id },
      process.env.JWT_SECRET,
      { expiresIn: '600s' }
    )
    return res.status(200).send({
      ..._.pick(user, ['id', 'name', 'lastName', 'email']),
      token
    })
  }
}

export default {
  register,
  login
}
