import Sale from '../models/sale.model.js'

const getList = async (req, res) => {
  const userId = req.query.user_id
  try {
    const rows = await Sale.getList(userId)
    return res.status(200).send(rows)
  } catch (err) {
    return res.status(500).send({ message: err.message })
  }
}

const create = async (req, res) => {
  const userId = req.userId
  const amount = req.body.amount
  if (isNaN(amount)) {
    return res.status(422).send({ message: '`amount` must be a number' })
  }
  try {
    const sale = await Sale.create(userId, amount)
    return res.status(201).send(sale)
  } catch (err) {
    return res.status(500).send({ message: err.message })
  }
}

const update = async (req, res) => {
  const amount = req.body.amount
  const saleId = req.params.sale_id
  if (isNaN(amount)) {
    return res.status(422).send({ message: '`amount` must be a number' })
  }
  try {
    const oldSale = await Sale.getById(saleId)
    if (!oldSale) {
      return res.status(404).send({ message: `Sale with id ${saleId} was not found` })
    }
    await Sale.update(saleId, amount)
    return res.status(200).send({
      ...oldSale,
      amount
    })
  } catch (err) {
    return res.status(500).send({ message: err.message })
  }
}

const remove = async (req, res) => {
  const saleId = req.params.sale_id
  try {
    const affectedRows = await Sale.remove(saleId)
    if (affectedRows === 0) {
      return res.status(404).send({ message: `Sale with id ${saleId} was not found` })
    }
    return res.status(200).send({})
  } catch (err) {
    return res.status(500).send({ message: err.message })
  }
}

export default {
  create,
  update,
  remove,
  getList
}
