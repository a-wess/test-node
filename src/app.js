import express from 'express'

import authRouter from './routers/auth.js'
import salesRouter from './routers/sales.js'

const app = express()

app.use(express.json())
app.use((err, _, res, next) => {
  if (err) {
    res.sendStatus(400)
  } else {
    next()
  }
})
app.use('/auth', authRouter)
app.use('/sales', salesRouter)

app.use((_, res) => {
  res.status(404).send({ message: 'Undefined route' })
})

export default app
