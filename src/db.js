import sqlite3 from 'sqlite3'

const sqlite = new sqlite3.Database(process.env.DB_FILE, err => {
  if (err) throw err
})

function promiseWrapper (call, resolver) {
  return (query, params) => new Promise((resolve, reject) => {
    sqlite[call](query, params, function (err, res) {
      if (err) reject(err)
      else resolve(resolver(res, this))
    })
  })
}

const insert = promiseWrapper('run', (_, context) => context.lastID)
const get = promiseWrapper('get', (res, _) => res)
const all = promiseWrapper('all', (res, _) => res)
const update = promiseWrapper('run', (_, context) => context.changes)

export default {
  insert,
  get,
  update,
  all
}
