import { Router } from 'express'
import SalesController from '../controllers/sale.controller.js'
import authVerification from '../utils/authVerification.js'

const router = new Router()

router.get('/', SalesController.getList)
router.post('/', authVerification, SalesController.create)
router.patch('/:sale_id', SalesController.update)
router.delete('/:sale_id', SalesController.remove)

export default router
