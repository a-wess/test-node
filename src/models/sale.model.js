import db from '../db.js'
/*
  Sale
    id: Number
    createdDate: Number
    amount Number
    userId Number
    deleted Number
*/

const create = async (userId, amount) => {
  const date = new Date().getTime()
  const id = await db.insert(
    'INSERT INTO sale VALUES(NULL, ?, ?, ?, 0)',
    [
      date,
      amount,
      userId
    ]
  )
  return {
    id,
    createdDate: date,
    amount,
    userId
  }
}

const update = (saleId, amount) => {
  return db.update(
    'UPDATE sale SET amount = ? WHERE id = ? AND deleted = 0',
    [
      amount,
      saleId
    ]
  )
}

const getById = (saleId) => {
  const query = `
    SELECT id, created_date as createdDate, amount, user_id as userId
    FROM sale
    WHERE id = ? and deleted = 0
  `
  return db.get(query, [saleId])
}

const getList = (userId) => {
  let query = `
    SELECT id, created_date as createdDate, amount, user_id as userId
    FROM sale
    WHERE deleted = 0
  `
  if (userId) {
    query += ' AND user_id = ?'
  }
  return db.all(query, [userId])
}

const remove = (saleId) => {
  return db.update(
    'UPDATE sale SET deleted = 1 WHERE id = ? and deleted = 0',
    [saleId]
  )
}

export default {
  create,
  update,
  remove,
  getById,
  getList
}
