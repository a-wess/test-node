import db from '../db.js'

/*
  User
    id: Number
    name: string
    lastName: string
    email: string
    password: string
*/

const create = async (user) => {
  const id = await db.insert(
    'INSERT INTO user VALUES(NULL, ?, ?, ?, ?)',
    [
      user.name,
      user.lastName,
      user.email,
      user.password
    ]
  )
  return {
    id,
    name: user.name,
    lastName: user.lastName,
    email: user.email
  }
}

const getByMail = async (email) => {
  const query = `
    SELECT
      id,
      name,
      last_name as lastName,
      email,
      password
    FROM user
    WHERE email = ?
  `
  const res = await db.get(query, [email])
  return res
}

export default {
  create,
  getByMail
}
