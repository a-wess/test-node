import 'dotenv/config'
import sqlite3 from 'sqlite3'

const createScriptText = `
  BEGIN TRANSACTION;
  DROP TABLE IF EXISTS user;
  DROP TABLE IF EXISTS sale;
  CREATE TABLE user(
    id  	INTEGER PRIMARY KEY AUTOINCREMENT,
    name    TEXT NOT NULL,
    last_name TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL
  );
  CREATE TABLE sale(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created_date INTEGER NOT NULL,
    amount INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    deleted INTEGER NOT NULL,
    FOREIGN KEY(user_id) REFERENCES user(id)
  );
  COMMIT;`

const queries = createScriptText.split(';').map(q => q.trim()).filter(q => q != '')
const db = new sqlite3.Database(process.env.DB_FILE, err => { if (err) throw err })

db.serialize(() => {
  queries.forEach(query => {
    db.run(query)
  })
})
