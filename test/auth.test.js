import request from "supertest"
import app from '../src/app.js'

describe("POST /auth/register", () => {
    describe("given correct form data", () => {
        test("should return user data and jwt token", async () => {
            const response = await request(app).post('/auth/register').send({
                name: 'Test',
                lastName: 'TestLname',
                email: 'test@test.com',
                password: '12345678'
            });
            expect(response.statusCode).toBe(201)
            expect(response.body).toEqual({
                id: expect.any(Number),
                name: expect.any(String),
                lastName: expect.any(String),
                email: expect.any(String),
                token: expect.any(String)
            })
        })
    })
    describe("given incorrect form data", () => {
        test(" (missing fields) should return 422", async () => {
            const response = await request(app).post('/auth/register').send({
                email: 'test@test.com',
                password: '12345678'
            });
            expect(response.statusCode).toBe(422)
        })
        test(" (missing fields) should return 422", async () => {
            const response = await request(app).post('/auth/register').send({});
            expect(response.statusCode).toBe(422)
        })
        test(" trying to register user with the same email twice", async () => {
            const response = await request(app).post('/auth/register').send({
                name: 'Test',
                lastName: 'TestLname',
                email: 'test@test.com',
                password: '12345678'
            });
            expect(response.statusCode).toBe(409)
        })
        test(" (incorrect email format) should return 422", async () => {
            const response = await request(app).post('/auth/register').send({
                name: 'Test',
                lastName: 'TestLname',
                email: 'test',
                password: '12345678'
            });
            expect(response.statusCode).toBe(422)
        })
    })
})

describe("POST /auth/login", () => {
    describe("given correct email and password", () => {
        test("should return user data and jwt token", async () => {
            const response = await request(app).post('/auth/login').send({
                email: 'test@test.com',
                password: '12345678'
            });
            expect(response.statusCode).toBe(200)
            expect(response.body).toEqual({
                id: expect.any(Number),
                name: expect.any(String),
                lastName: expect.any(String),
                email: expect.any(String),
                token: expect.any(String)
            })
        })
    })
    describe("given incorrect data", () => {
        test(" (no email) return 422", async () => {
            const response = await request(app).post('/auth/login').send({
                email: '',
                password: '12345678'
            });
            expect(response.statusCode).toBe(422)
        })
        test(" (incorrect email format) return 422", async () => {
            const response = await request(app).post('/auth/login').send({
                email: 'test',
                password: '12345678'
            });
            expect(response.statusCode).toBe(422)
        })

        test(" (no password) return 422", async () => {
            const response = await request(app).post('/auth/login').send({
                email: 'auyez@gmail.com',
                password: ''
            });
            expect(response.statusCode).toBe(422)
        })

        test(" (incorrect data type) return 422", async () => {
            const response = await request(app).post('/auth/login').send({
                email: 12345,
                password: 8009
            });
            expect(response.statusCode).toBe(422)
        })

        test(" (incorrect data type) return 422", async () => {
            const response = await request(app).post('/auth/login').send({
                emaile: 12345,
                passwordsd: 8009
            });
            expect(response.statusCode).toBe(422)
        })
    })
    describe("given non-existing user", () => {
        test(" should return 403", async () => {
            const response = await request(app).post('/auth/login').send({
                email: 'auyez@gmail.com',
                password: 'asodkpasd'
            });
            expect(response.statusCode).toBe(403)
        })
    })
})