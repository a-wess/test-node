import request from "supertest"
import app from '../src/app.js'

var regToken = ''
var authToken = ''

var firstUser = {
    token: null,
    id: null
}
var secondUser = {
    token: null,
    id: null,
}

beforeAll(async () => {
    const register = await request(app).post('/auth/register').send({
        name: 'L',
        lastName: 'D',
        email: 'ld@p.com',
        password: '12345678'
    });
    const auth = await request(app).post('/auth/login').send({
        email: 'ld@p.com',
        password: '12345678'
    })
    regToken = register.body.token
    authToken = auth.body.token

    const second = await request(app).post('/auth/register').send({
        name: 'T',
        lastName: 'G',
        email: 'tg@p.com',
        password: '12345678'
    });

    firstUser.id = register.body.id
    firstUser.token = authToken

    secondUser.id = second.body.id
    secondUser.token = second.body.token
})

describe("POST /sales", () => {
    test(" with token received during registration should return 201", async () => {
        const response = await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${regToken}`)
            .send({ amount: 1000 })
        expect(response.statusCode).toBe(201)
        expect(response.body).toEqual({
            id: expect.any(Number),
            createdDate: expect.any(Number),
            amount: expect.any(Number),
            userId: expect.any(Number)
        })
    })

    test(" with token received during auth should return 201", async () => {
        const response = await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${authToken}`)
            .send({ amount: 2000 })
        expect(response.statusCode).toBe(201)
        expect(response.body).toEqual({
            id: expect.any(Number),
            createdDate: expect.any(Number),
            amount: expect.any(Number),
            userId: expect.any(Number)
        })
    })

    test(" with empty payload should return 422", async () => {
        const response = await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${authToken}`)
            .send({})
        expect(response.statusCode).toBe(422)
    })

    test(" payload with wrong type should return 422", async () => {
        const response = await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${authToken}`)
            .send({ amount: 'test' })
        expect(response.statusCode).toBe(422)
    })

    test(" payload with wrong field name should return 422", async () => {
        const response = await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${authToken}`)
            .send({ amountx: 'test' })
        expect(response.statusCode).toBe(422)
    })

    test(" without a jwt token", async () => {
        const response = await request(app)
            .post('/sales')
            .send({ amount: 2000 })
        expect(response.statusCode).toBe(403)
    })

    test(" with invalid jwt token", async () => {
        const response = await request(app)
            .post('/sales')
            .set('Authorization', `Bearer asddgijsdoigjiodjvoidfjso12312`)
            .send({ amount: 2000 })
        expect(response.statusCode).toBe(403)
    })
})

describe("PATCH /sales/<sale_id>", () => {
    let sale = null
    let deletedId = null
    beforeAll(async () => {
        sale = (await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${regToken}`)
            .send({ amount: 2000 })).body
        deletedId = (await request(app)
            .post('/sales')
            .set('Authorization', `Bearer ${regToken}`)
            .send({ amount: 5000 })).body.id
        await request(app).delete(`/sales/${deletedId}`)
    })

    test(" with correct input should return 200", async () => {
        const response = await request(app)
            .patch(`/sales/${sale.id}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send({ amount: 1000 })
        expect(response.statusCode).toBe(200)
        expect(response.body.id).toBe(sale.id)
        expect(response.body).toEqual({
            id: expect.any(Number),
            createdDate: expect.any(Number),
            amount: 1000,
            userId: expect.any(Number)
        })
    })

    test("trying to update non existing sale should return 404", async () => {
        const response = await request(app)
            .patch(`/sales/1234`)
            .set('Authorization', `Bearer ${authToken}`)
            .send({ amount: 1000 })
        expect(response.statusCode).toBe(404)
    })

    test("trying to update soft deleted sale should return 404", async () => {
        const response = await request(app)
            .patch(`/sales/${deletedId}`)
            .set('Authorization', `Bearer ${authToken}`)
            .send({ amount: 67000 })
        expect(response.statusCode).toBe(404)
    })

    test(" without a jwt token", async () => {
        const response = await request(app)
            .patch(`/sales/${sale.id}`)
            .send({ amount: 9000 })
        expect(response.statusCode).toBe(200)
        expect(response.body).toEqual({
            id: sale.id,
            createdDate: expect.any(Number),
            amount: 9000,
            userId: expect.any(Number)
        })
    })
})

describe("DELETE /sales/<sale_id>", () => {
    let created = []
    beforeAll(async () => {
        for (let i = 0; i < 5; i++) {
            const resp = await request(app)
                .post('/sales')
                .set('Authorization', `Bearer ${authToken}`)
                .send({ amount: i * 100 })
            created.push(resp.body.id)
        }
    })

    test("delete correctly with valid id", async () => {
        const resp = await request(app).delete(`/sales/${created[0]}`)
        expect(resp.statusCode).toBe(200)
        expect(resp.body).toEqual({})
    })

    test("delete with invalid id", async () => {
        const resp = await request(app).delete('/sales/1234566')
        expect(resp.statusCode).toBe(404)
    })

    test("delete with empty param", async () => {
        const resp = await request(app).delete('/sales/')
        expect(resp.statusCode).toBe(404)
    })

    test("try to delete second time", async () => {
        const first = await request(app).delete(`/sales/${created[1]}`)
        expect(first.statusCode).toBe(200)
        expect(first.body).toEqual({})
        const second = await request(app).delete(`/sales/${created[1]}`)
        expect(second.statusCode).toBe(404)
    })
})

describe("GET /sales/", () => {
    beforeAll(async () => {
        for(let i = 0; i < 10; i++) {
            await request(app)
                .post('/sales')
                .set('Authorization', `Bearer ${firstUser.token}`)
                .send({ amount: i * 10})
        }
        for(let i = 0; i < 5; i++) {
            await request(app)
                .post('/sales')
                .set('Authorization', `Bearer ${secondUser.token}`)
                .send({ amount: i * 10})
        }
    })

    test(" number of sales should be more than 15", async () => {
        const resp = await request(app).get('/sales')
        expect(resp.statusCode).toBe(200)
        expect(resp.body.length).toBeGreaterThanOrEqual(15)
        expect(resp.body).toEqual(expect.arrayContaining([
            {
                id: expect.any(Number),
                amount: expect.any(Number),
                userId: expect.any(Number),
                createdDate: expect.any(Number)
            }
        ]))
    })

    test(" number of sales of the second user should equal 5", async () => {
        const resp = await request(app).get(`/sales?user_id=${secondUser.id}`)
        expect(resp.statusCode).toBe(200)
        expect(resp.body.length).toBe(5)
        expect(resp.body).toEqual(expect.arrayContaining([
            {
                id: expect.any(Number),
                amount: expect.any(Number),
                userId: expect.any(Number),
                createdDate: expect.any(Number)
            }
        ]))
    })
})