import 'dotenv/config'
import app from './src/app.js'

app.listen(process.env.PORT)

console.log(`Started server at port ${process.env.PORT}`)
